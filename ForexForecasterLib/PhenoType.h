#include "common.h"
#include "Net.h"
#include "GenoType.h"

class PhenoType {
public:
	PhenoType(GenoType &genotype);
	void GenerateTrainingData(CurrencyData currency_data);
	void TrainNeuralNet();
	void TestNeuralNet();
	void saveNeuralNetwork(string filename);
	void loadNeuralNetwork(string filename);

private:
	void seperateTestData();
	GenoType *genotype;
	Net *neural_net;
	TrainingSet training_data;
	TrainingSet test_data;
	TestResult test_result;
};