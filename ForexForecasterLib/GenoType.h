#include "common.h"

using namespace std;

#ifndef GENOTYPE_H
#define GENOTYPE_H

class GenoType {
public:
	GenoType();
	GenoType(GenoType p1, GenoType p2);
	void Mutate();
	vector<unsigned> Topology();
	double getLearningRate() { return learning_rate; };
	void setLearningRate(double learning_rate) { this->learning_rate = learning_rate; };
	double getMomentum() { return momentum; };
	void setMomentum(double momentum) { this->momentum = momentum; };
	TestResult getTestResult() { return test_result; };
	void setTestResult(TestResult test_result) { this->test_result = test_result; };

	bool operator<(const GenoType& val) const { return test_result.fitness < val.test_result.fitness; };

private:
	double mutation_chance = 0.3;

	//topology settings used in neural network and training data generation
	unsigned t_in;
	unsigned t_h1;
	unsigned t_h2;

	//neural network settings
	double learning_rate;
	double momentum;

	//fittnes_results
	TestResult test_result;
};

#endif