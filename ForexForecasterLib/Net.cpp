#include "Net.h"
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

void Net::feedForward(vector<double> inputVals) {
	if (inputVals.size() != m_layers[0].size() - 1) {
		BOOST_LOG_TRIVIAL(info) << "Input size and input neuron count mismatch. Unable to perform feedForward.";
		return;
	}

	//assign latch input vals to input neurons
	for (unsigned i = 0; i < inputVals.size(); ++i) {
		m_layers[0][i].setOutputVal(inputVals[i]);
	}

	//Forward propagate
	for (unsigned layerNum = 1; layerNum < m_layers.size(); ++layerNum) {
		Layer &prevLayer = m_layers[layerNum - 1];
		for (unsigned n = 0; n < m_layers[layerNum].size() - 1; ++n) {
			m_layers[layerNum][n].feedForward(prevLayer);
		}
	}
}

void Net::backProp(vector<double> targetVals) {
	//calculate overall net error (RMS of output neuron errors)
	Layer &outputLayer = m_layers.back();
	m_error = 0;

	for (unsigned n = 0; n < outputLayer.size() - 1; ++n) {
		double delta = targetVals[n] - outputLayer[n].getOutputVal();
		m_error += delta*delta;
	}

	m_error /= outputLayer.size() - 1;//average error squared
	m_error = sqrt(m_error);//RMS	

	//calculate output layer gradients
	for (unsigned n = 0; n < outputLayer.size() - 1; ++n) {
		outputLayer[n].calcOutputGradients(targetVals[n]);
	}

	//calculate gradients on hidden layers
	for (unsigned layerNum = m_layers.size() - 2; layerNum > 0; --layerNum) {
		Layer &hiddenLayer = m_layers[layerNum];
		Layer &nextLayer = m_layers[layerNum + 1];

		for (unsigned n = 0; n < hiddenLayer.size(); ++n) {
			hiddenLayer[n].calcHiddenGradients(nextLayer);
		}
	}

	//for all layers from outputs to first hidden layer
	//update connection weights
	for (unsigned layerNum = m_layers.size() - 1; layerNum > 0; --layerNum) {
		Layer &layer = m_layers[layerNum];
		Layer &prevLayer = m_layers[layerNum - 1];
		for (unsigned n = 0; n < layer.size() - 1; ++n) {
			layer[n].updateInputWeights(prevLayer);
		}
	}
}

void Net::Train(TrainingSet training_data) {
	for (auto item : training_data) {
		feedForward(item.inputs);
		backProp({ item.output });
	}
}

Net::Net(Topology topology) {
	init(topology);
}

Net::Net(string filename) {
	loadFromFile(filename);
}

Net::Net(const wchar_t *filename) {
	loadFromFile(filename);
}

Net::Net(Topology topology, double learning_rate, double momentum) {
	this->learning_rate = learning_rate;
	this->momentum = momentum;
	init(topology);
}

void Net::init(const Topology topology) {
	this->topology = topology;
	m_layers.clear();
	for (unsigned layerNum = 0; layerNum < topology.size(); layerNum++) {
		m_layers.push_back(Layer());
		unsigned numOutputs = layerNum == topology.size() - 1 ? 0 : topology[layerNum + 1];

		for (unsigned neuronNum = 0; neuronNum <= topology[layerNum]; ++neuronNum) {
			m_layers.back().push_back(Neuron(numOutputs, neuronNum, learning_rate, momentum));
		}
		m_layers.back().back().setOutputVal(1.0);
	}
}

vector<double> Net::getResults() {
	vector<double> return_values;
	for (unsigned n = 0; n < m_layers.back().size() - 1; ++n) {
		return_values.push_back(m_layers.back()[n].getOutputVal());
	}
	return return_values;
}

void Net::saveStateToFile(string filename) {
	std::filebuf fb;
	fb.open(filename, std::ios::out);
	std::ostream os(&fb);
	for (size_t i = 0; i < this->m_layers.size(); i++) {
		os << this->m_layers[i].size();
		if (i != this->m_layers.size() - 1)
			os << " ";
	}
	os << endl;

	for (size_t i = 0; i < this->m_layers.size() - 1; i++) { //Last layer is the output layer, so no outward connections
		for each (Neuron neuron in this->m_layers[i]) {
			vector<Connection> weights = neuron.getCurrentWeights();
			for (size_t w = 0; w < weights.size(); w++) {
				os << weights[w].weight;
				if (w != weights.size() - 1)
					os << " ";
			}
			os << endl;
			for (size_t j = 0; j < weights.size(); j++) {
				os << weights[j].deltaWeight;
				if (j != weights.size() - 1)
					os << " ";
			}
			if (i != this->m_layers.size() - 1)
				os << endl;
		}
	}

	//cout << "Connection weights wrote to file" << filename.c_str() << "." << endl;
	fb.close();

}

void Net::loadFromFile(string filename) {
	ifstream saved_net_file;
	saved_net_file.open(filename);
	loadFromFile(saved_net_file);
}

void Net::loadFromFile(const wchar_t *filename) {
	ifstream saved_net_file;
	saved_net_file.open(filename);
	loadFromFile(saved_net_file);
}

void Net::loadFromFile(ifstream& saved_net_file) {
	string line;
	vector<unsigned> topology;
	
	getline(saved_net_file, line);
	stringstream ss(line);
	if (saved_net_file.eof()) {
		return;
	}

	while (!ss.eof()) {
		unsigned n;
		ss >> n;
		topology.push_back(n - 1);
	}

	init(topology);

	//loading the weights from file
	for (unsigned i = 0; i < m_layers.size() - 1; i++) {
		for (unsigned j = 0; j < m_layers[i].size(); ++j) {
			getline(saved_net_file, line);

			//connection weights for neuron
			stringstream ss(line);

			vector<Connection> connections;
			vector<double> weights;
			vector<double> deltaWeights;
			while (!ss.eof()) {
				double n;
				ss >> n;
				weights.push_back(n);
			}
			getline(saved_net_file, line);

			//connection deltaweights for neuron
			stringstream ss2(line);
			while (!ss2.eof()) {
				double n;
				ss2 >> n;
				deltaWeights.push_back(n);
			}
			for (size_t k = 0; k < weights.size(); k++) {
				Connection con;
				con.weight = weights[k];
				con.deltaWeight = deltaWeights[k];
				connections.push_back(con);
			}

			m_layers[i][j].setOutputWeights(connections);
		}
	}
}