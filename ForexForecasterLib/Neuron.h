#include "common.h"

using namespace std;

class Neuron {
public:
	Neuron(unsigned numOutputs, unsigned myIndex);
	Neuron(unsigned numOutputs, unsigned myIndex, double learning_rate, double momentum);
	void setOutputVal(double val) { m_outputVal = val; }
	double getOutputVal(void)const { return m_outputVal; }
	void feedForward(const Layer &prevLayer);
	void calcOutputGradients(double targetVal);
	void calcHiddenGradients(const Layer &nextLayer);
	void updateInputWeights(Layer &prevLayer);
	vector<Connection> getCurrentWeights()const { return m_outputWeights; }
	void setOutputWeights(vector<Connection> weights);
private:
	void init(unsigned numOutputs, unsigned myIndex, double learning_rate, double momentum);
	static double transferFunction(double x);
	static double transferFunctionDerivative(double x);
	static double randomWeight();
	double learning_rate;
	double momentum;
	double sumDOW(const Layer &nextLayer)const;
	double m_outputVal;
	vector<Connection>	m_outputWeights;
	unsigned m_myIndex;
	double m_gradient;
};