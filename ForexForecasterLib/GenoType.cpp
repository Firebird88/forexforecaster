#include "GenoType.h"
#include <random>

//generate new
GenoType::GenoType() {
	this->t_in = rand() % 12 + 10;
	this->t_h1 = rand() % 13 + 8;
	this->t_h2 = rand() % 10 + 8;
	this->learning_rate = 0.1 + (rand() / double(RAND_MAX))*0.3;
	this->momentum = 0.1 + (rand() / double(RAND_MAX))*0.3;
}

//crossover
GenoType::GenoType(GenoType p1, GenoType p2) {
	this->t_in = rand() % 2 == 0 ? p1.t_in : p2.t_in;
	this->t_h1 = rand() % 2 == 0 ? p1.t_h1 : p2.t_h1;
	this->t_h2 = rand() % 2 == 0 ? p1.t_h2 : p2.t_h2;
	this->learning_rate = rand() % 2 == 0 ? p1.learning_rate : p2.learning_rate;
	this->momentum = rand() % 2 == 0 ? p1.momentum : p2.momentum;
}

//mutation
void GenoType::Mutate() {
	if (rand() / double(RAND_MAX) <= mutation_chance) {
		switch (rand() % 5) {
		case 4:	t_in += rand() % 7 - 3;	break;
		case 3:	t_h1 += rand() % 7 - 3;	break;
		case 2:	t_h2 += rand() % 7 - 3;	break;
		case 1:	learning_rate += (rand() / double(RAND_MAX) - 0.5) / 5; break;
		case 0:	momentum += (rand() / double(RAND_MAX) - 0.5) / 5; break;
		}
		BOOST_LOG_TRIVIAL(info) << "Mutation occured.";
	}
}

Topology GenoType::Topology() {
	return vector < unsigned > {t_in, t_h1, t_h2, 1}; //output neuron count is 1 everytime
}