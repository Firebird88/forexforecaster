#include "common.h"
#include "Neuron.h"

using namespace std;

#define DEFAULT_LEARNING_RATE 0.15
#define DEFAULT_MOMENTUM 0.5

class Net {
public:
	Net(Topology topology);
	Net(Topology topology, double learning_rate, double momentum);
	Net(string filename);
	Net(const wchar_t *filename);

	void init(Topology topology);
	void feedForward(vector<double> inputVals);
	void backProp(vector<double> targetVals);
	void Train(TrainingSet training_data);
	vector<double> getResults();
	double getLastError() { return m_error; }

	void setTopology(Topology topology) { this->topology = topology; };
	Topology getTopology() { return this->topology; };
	double getLearningRate() { return learning_rate; };
	void setLearningRate(double learning_rate) { this->learning_rate = learning_rate; }; //currently not working since learning rate is fixed in neuron creation
	double getMomentum() { return momentum; };
	void setMomentum(double momentum) { this->momentum = momentum; }; //not working similarily to learning rate

	void saveStateToFile(string filename);
	void loadFromFile(string filename);
	void loadFromFile(const wchar_t *filename);
	void loadFromFile(ifstream& saved_net_file);

private:
	Topology topology;
	vector<Layer> m_layers;
	double m_error;

	double learning_rate = DEFAULT_LEARNING_RATE;
	double momentum = DEFAULT_MOMENTUM;
};
