#include "Neuron.h"
#include <random>

using namespace std;

#define DEFAULT_LEARNING_RATE 0.15
#define DEFUALT_MOMENTUM 0.5

Neuron::Neuron(unsigned numOutputs, unsigned myIndex) {
	init(numOutputs, myIndex, DEFAULT_LEARNING_RATE, DEFUALT_MOMENTUM);
}
Neuron::Neuron(unsigned numOutputs, unsigned myIndex, double learning_rate, double momentum) {
	init(numOutputs, myIndex, learning_rate, momentum);
}

void Neuron::init(unsigned numOutputs, unsigned myIndex, double learning_rate, double momentum) {
	this->learning_rate = learning_rate;
	this->momentum = momentum;
	for (unsigned c = 0; c < numOutputs; c++) {
		m_outputWeights.push_back(Connection());
		m_outputWeights.back().weight = randomWeight();
	}
	m_myIndex = myIndex;
}

void Neuron::updateInputWeights(Layer &prevLayer) {
	for (unsigned n = 0; n < prevLayer.size(); n++) {
		Neuron &neuron_in = prevLayer[n];
		double oldDeltaWeight = neuron_in.m_outputWeights[m_myIndex].deltaWeight;
		double newDeltaWeight =
			learning_rate
			*neuron_in.getOutputVal()
			*m_gradient
			+ momentum
			*oldDeltaWeight;

		neuron_in.m_outputWeights[m_myIndex].deltaWeight = newDeltaWeight;
		neuron_in.m_outputWeights[m_myIndex].weight += newDeltaWeight;
	}
}

double Neuron::sumDOW(const Layer &nextLayer)const {
	double sum = 0.0;
	for (unsigned n = 0; n < nextLayer.size() - 1; ++n) {
		sum += m_outputWeights[n].weight*nextLayer[n].m_gradient;
	}

	return sum;
}

void Neuron::calcHiddenGradients(const Layer &nextLayer) {
	double dow = sumDOW(nextLayer);
	m_gradient = dow*Neuron::transferFunctionDerivative(m_outputVal);
}

void Neuron::calcOutputGradients(double targetVal) {
	double delta = targetVal - m_outputVal;
	m_gradient = delta*Neuron::transferFunctionDerivative(m_outputVal);
}

double Neuron::transferFunction(double x) {
	//tanh -1 <-> +1
	//return tanh(x);
	return x / (1 + abs(x));

}
double Neuron::transferFunctionDerivative(double x) {
	//tanh derivative
	//return (1.0 - x)*x;
	return (pow(1.0 - abs(x), 2));
}

void Neuron::feedForward(const Layer &prevLayer) {
	double sum = 0.0;

	for (unsigned n = 0; n < prevLayer.size(); ++n) {
		sum += prevLayer[n].getOutputVal()*prevLayer[n].m_outputWeights[m_myIndex].weight;
	}

	m_outputVal = Neuron::transferFunction(sum);
}

void Neuron::setOutputWeights(vector<Connection> weights) {
	m_outputWeights = weights;
}

double Neuron::randomWeight() {
	random_device rd;
	mt19937 gen(rd());
	uniform_real_distribution<> dis(-0.1, 0.1);
	return dis(gen);
}