#include <Windows.h>

class StopWatch {
public:
	StopWatch();
	void Start();
	double ElapsedTime();
	double Stop();

private:
	LARGE_INTEGER frequency;
	LARGE_INTEGER t1, t2;
};