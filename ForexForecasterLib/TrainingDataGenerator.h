#include "common.h"

using namespace std;

#ifndef TRAININGDATAGENERATOR_H
#define TRAININGDATAGENERATOR_H

#define INPUT_COUNT	20

class TrainingDataGenerator {
public:
	TrainingDataGenerator(string resource);
	TrainingDataGenerator(CurrencyData &currency_data);
	void loadCurrencyDataFromFile(string resource);
	void setConstructionParams(unsigned prediction_interval, unsigned step_size);
	void setTopology(Topology topology);
	TrainingSet calculateTrainingData();
	CurrencyData getCurrencyData() { return currency_data; };

private:
	vector<string> getFileNames(string folder_name);

	CurrencyData currency_data;
	TrainingSet training_data;
	Topology topology;

	int input_count = INPUT_COUNT;

};

#endif