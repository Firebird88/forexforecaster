#include "StopWatch.h"
#include <iostream>

using namespace std;

StopWatch::StopWatch() {
	QueryPerformanceFrequency(&frequency);
}

void StopWatch::Start() {
	QueryPerformanceCounter(&t1);
}

double StopWatch::ElapsedTime() {
	QueryPerformanceCounter(&t2);
	return (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
}

double StopWatch::Stop() {
	double elapsedTime = ElapsedTime();
	cout << endl << "Measured time: " << elapsedTime << "ms" << endl;
	return elapsedTime;
}
