#include "StopWatch.h"
#include "TrainingDataGenerator.h"
#include <fstream>

TrainingDataGenerator::TrainingDataGenerator(string resource) {
	loadCurrencyDataFromFile(resource);
}

TrainingDataGenerator::TrainingDataGenerator(CurrencyData &currency_data) {
	this->currency_data = currency_data;
}

void TrainingDataGenerator::setTopology(Topology topology) {
	this->topology = topology;
	this->input_count = topology.front();
}

void TrainingDataGenerator::loadCurrencyDataFromFile(string resource) {
	ifstream fin(resource);
	if (!fin) {
		BOOST_LOG_TRIVIAL(info) << "Error opening the file " << resource;
		return;
	}
	BOOST_LOG_TRIVIAL(info) << "File open. Walking through \"" << resource << "\".";

	while (true) {
		RateData rate_data;
		fin >> rate_data.timestamp >> rate_data.open >> rate_data.high >> rate_data.low >> rate_data.close >> rate_data.tick_volume >> rate_data.year >> rate_data.month
			>> rate_data.day >> rate_data.hour >> rate_data.min >> rate_data.day_of_week >> rate_data.day_of_year >> rate_data.ind_ma;
		if (fin.eof()) break;
		if (fin.fail()) {
			BOOST_LOG_TRIVIAL(info) << "Error: Bad file structure in \"" << resource << "\".";
			break;
		} else {
			this->currency_data.push_back(rate_data);
		}
	}
}

TrainingSet TrainingDataGenerator::calculateTrainingData() {
	StopWatch sw;
	sw.Start();

	int training_data_needed = 4 * (this->topology[0] * this->topology[1] + this->topology[1] * this->topology[2] + this->topology[2] * this->topology[3]);
	for (size_t current_position = this->currency_data.size() - 2; current_position > this->currency_data.size() - training_data_needed; current_position--) {
		TrainingItem item;

		vector<double> inputs;
		int fib_1 = 0;
		int fib_2 = 1;

		for (size_t i = 0; i < this->input_count; i++) {
			int fib = fib_1 + fib_2;
			fib_1 = fib_2;
			fib_2 = fib;
			
			double in = this->currency_data[current_position].open / this->currency_data[current_position - fib].open - 1;
			inputs.push_back(in);
		}

		item.inputs = inputs;

		double output = this->currency_data[current_position].open / this->currency_data[current_position+1].open - 1;
		item.output = output;

		this->training_data.push_back(item);
	}
	BOOST_LOG_TRIVIAL(info) << "";
	BOOST_LOG_TRIVIAL(info) << "Training data calculated for: ("<<this->training_data.size()<<")";
	BOOST_LOG_TRIVIAL(info) << "--Input size : " << input_count << " in " << sw.ElapsedTime() << "ms";
	
	return this->training_data;
}
