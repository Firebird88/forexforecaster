#include "PhenoType.h"
#include "TrainingDataGenerator.h"
#include <sstream>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>

using namespace std;
using namespace boost::accumulators;

PhenoType::PhenoType(GenoType &genotype) {
	this->genotype = &genotype;
	this->neural_net = new Net(this->genotype->Topology(), this->genotype->getLearningRate(), this->genotype->getMomentum());
}

void PhenoType::GenerateTrainingData(CurrencyData currency_data) {
	TrainingDataGenerator generator(currency_data);

	generator.setTopology(genotype->Topology());
	this->training_data = generator.calculateTrainingData();

	seperateTestData();
}

void PhenoType::loadNeuralNetwork(string filename) {
	this->neural_net->loadFromFile(filename);
}

void PhenoType::TrainNeuralNet() {	
	this->neural_net->Train(this->training_data);
	BOOST_LOG_TRIVIAL(info) << "Neural Network settings:";
	BOOST_LOG_TRIVIAL(info) << "--Learning rate: " << this->neural_net->getLearningRate() << " Momentum: " << this->neural_net->getMomentum() << " Topology: " << this->genotype->Topology().at(0) << " " << this->genotype->Topology().at(1) << " " << this->genotype->Topology().at(2) << " 1";
}

void PhenoType::TestNeuralNet() {
	vector<double> test_errors;
	int negative_prediction_cnt = 0;
	int bad_heading_prediction_cnt = 0;
	for (auto test_item : test_data) {
		vector<double> results;
		this->neural_net->feedForward(test_item.inputs);
		results = this->neural_net->getResults();
		test_errors.push_back(this->neural_net->getLastError());
		if (signbit(test_item.output) != signbit(results[0])) bad_heading_prediction_cnt++;
		if (signbit(results[0]))negative_prediction_cnt++;
	}	
	
	accumulator_set<double, stats<tag::variance> > acc;
	for_each(test_errors.begin(), test_errors.end(), bind<void>(ref(acc), std::placeholders::_1));
	test_result.error_average = mean(acc);
	test_result.error_variance = variance(acc);

	test_result.fitness = mean(acc);

	BOOST_LOG_TRIVIAL(info) << "Test results: (" << test_data.size() << ")";
	BOOST_LOG_TRIVIAL(info) << "--Error average: " << test_result.error_average << " Error variance:" << test_result.error_variance;
	BOOST_LOG_TRIVIAL(info) << "--Bad heading predictions: " << bad_heading_prediction_cnt << " Negative heading predictions:" << negative_prediction_cnt;
	BOOST_LOG_TRIVIAL(info) << "--------------------------------------------------------------------------------";

	genotype->setTestResult(test_result);
}

void PhenoType::seperateTestData() {
	for (size_t i = 0; i < training_data.size(); i += 10) {
		unsigned offset = rand() % 10;
		offset = i + offset >= training_data.size() ? training_data.size() - 1 - i : offset;
		test_data.push_back(training_data.at(i + offset));
		training_data.erase(training_data.begin() + i + offset);
	}
}

void PhenoType::saveNeuralNetwork(string filename) {
	this->neural_net->saveStateToFile(filename);
}