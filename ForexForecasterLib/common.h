#include <vector>
#include <boost/log/trivial.hpp>

using namespace std;

#ifndef COMMON_H
#define COMMON_H

struct RateData {
	int timestamp;
	double open;
	double high;
	double low;
	double close;
	int tick_volume;
	int year;
	int month;
	int day;
	int hour;
	int min;
	int day_of_week;
	int day_of_year;
	double ind_ma;

	bool operator < (const RateData& str) const {
		return (timestamp < str.timestamp);
	}
};

struct Connection {
	double weight;
	double deltaWeight;
};

struct TrainingItem {
	vector<double> inputs;
	double output;
};

struct TestResult {
	double fitness;
	double error_average;
	double error_variance;
};

class Neuron;

typedef vector<RateData> CurrencyData;
typedef vector<TrainingItem> TrainingSet;
typedef vector<unsigned> Topology;
typedef vector<Neuron> Layer;

#endif