#include "common.h"
#include "GenoType.h"
#include "PhenoType.h"
#include "TrainingDataGenerator.h"

#include <boost/log/expressions.hpp>
#include <boost/log/expressions/keyword.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

using namespace boost::log;
using namespace boost::posix_time;
using namespace boost::filesystem;

BOOST_LOG_ATTRIBUTE_KEYWORD(timestamp, "TimeStamp", boost::posix_time::ptime);

void initLogger(string filename) {
	boost::log::add_common_attributes();

	boost::log::add_file_log(
		keywords::file_name = filename + ".log",
		keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
		keywords::format = (
		boost::log::expressions::stream << boost::log::expressions::format_date_time(timestamp, "[%Y-%b-%d %H:%M:%S]: ") << boost::log::expressions::smessage
		),
		keywords::auto_flush = true
		);

	boost::log::core::get()->set_filter(
		boost::log::trivial::severity >= boost::log::trivial::info
		);
}

stringstream logName() {
	time_facet *facet = new time_facet("%Y%m%d_%H_%M_%S");

	stringstream log_name;
	log_name.imbue(locale(cout.getloc(), facet));
	log_name << second_clock::local_time();

	return log_name;
}

int main(int argc, char *argv[]) {
	string output_filename = logName().str();
	initLogger(output_filename);
	int generation_count = 500;
	int generation_size = 50;

	string work_dir = boost::filesystem::current_path().string();

	string input_filename;
	if (argc >= 2) {
		input_filename = argv[1];
	} else {
		input_filename = "EURUSD_M15_NEW.TXT";
	}

	if (argc > 2) {
		if (strtol(argv[2], NULL, 0) != 0L)
			generation_count = strtol(argv[2], NULL, 0);
	}

	TrainingDataGenerator generator(input_filename);
	CurrencyData currency_data = generator.getCurrencyData();

	vector<GenoType> last_generation, best_overall;

	BOOST_LOG_TRIVIAL(info) << "--Input file: " << input_filename;
	BOOST_LOG_TRIVIAL(info) << "--Starting genetic algorithm. Generation size:" << generation_size << " Generation count:" << generation_count;
	BOOST_LOG_TRIVIAL(info) << "";

	for (auto generation = 0; generation < generation_count; generation++) {
		cout << "" << generation << " of " << generation_count << " generations." << endl;
		vector<GenoType> genotype_generation;

		if (last_generation.size()>0) {
			genotype_generation.insert(genotype_generation.end(), last_generation.begin(), last_generation.end());
			BOOST_LOG_TRIVIAL(info) << "--Including the children of the last generation. (" << last_generation.size() << ")";
		}
		for (size_t i = 0; i < generation_size - last_generation.size(); i++) {
			GenoType genotype;
			genotype_generation.push_back(genotype);
		}
		BOOST_LOG_TRIVIAL(info) << "--Genotype generation number " << generation + 1 << " created.";

		BOOST_LOG_TRIVIAL(info) << "--Creating phenotype generation based on the genotypes.";
		vector<PhenoType> phenotype_generation;
		for (size_t i = 0; i < genotype_generation.size(); i++) {
			PhenoType phenotype(genotype_generation.at(i));
			phenotype.GenerateTrainingData(currency_data);
			phenotype.TrainNeuralNet();
			phenotype.TestNeuralNet();
			phenotype_generation.push_back(phenotype);
			boost::filesystem::path dir(output_filename);
			boost::filesystem::path dir_sub(output_filename+"\\all");
			if ((boost::filesystem::create_directory(dir) || boost::filesystem::is_directory(dir)) && (boost::filesystem::create_directory(dir_sub) || boost::filesystem::is_directory(dir_sub))) {
				phenotype.saveNeuralNetwork(work_dir + "\\" + output_filename + "\\all\\" + to_string((generation*genotype_generation.size()) + i + 1) + ".net");
			} else {
				BOOST_LOG_TRIVIAL(info) << "Error creating folder.";
			}
		}
		BOOST_LOG_TRIVIAL(info) << "--Phenotype generation created.";
		BOOST_LOG_TRIVIAL(info) << "--Sorting by fitness.";

		std::sort(genotype_generation.begin(), genotype_generation.end());

		BOOST_LOG_TRIVIAL(info) << "";
		BOOST_LOG_TRIVIAL(info) << "--The top 5 fitness values this generation:";
		for (size_t i = 0; i < 5; i++) {
			BOOST_LOG_TRIVIAL(info) << i + 1 << ": " << genotype_generation[i].getTestResult().error_average;
		}

		best_overall.push_back(genotype_generation[0]);

		if (generation < generation_count - 1) {
			BOOST_LOG_TRIVIAL(info) << "--Recombining the elitest 30% and saving them for the next generation. (" << size_t(genotype_generation.size()*0.2) << ")";
			last_generation.clear();
			for (size_t i = 0; i < size_t(genotype_generation.size()*0.3) - 1; i++) {
				GenoType child(genotype_generation.at(i), genotype_generation.at(i + 1));
				last_generation.push_back(child);
			}

			BOOST_LOG_TRIVIAL(info) << "--Mutating the children.";
			for (auto child : last_generation) {
				child.Mutate();
			}

			BOOST_LOG_TRIVIAL(info) << "--Moving on to the next generation.";
			BOOST_LOG_TRIVIAL(info) << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
		} else {
			BOOST_LOG_TRIVIAL(info) << "";
			BOOST_LOG_TRIVIAL(info) << "--Last generation finished evaluating. The best results throughout the running of the algorithm:";
			BOOST_LOG_TRIVIAL(info) << "Originally calculated error, regenerated error, and reimported error:";
			std::sort(best_overall.begin(), best_overall.end());
			for (size_t i = 0; i < 5; i++) {
				BOOST_LOG_TRIVIAL(info) << i + 1 << ": " << best_overall[i].getTestResult().error_average;

				BOOST_LOG_TRIVIAL(info) << "Regenerating and retesting phenotype based on genotype.";
				PhenoType phenotype(best_overall.at(i));
				phenotype.GenerateTrainingData(currency_data);
				phenotype.TrainNeuralNet();
				phenotype.TestNeuralNet();
				BOOST_LOG_TRIVIAL(info) << "Saving net to " << work_dir + "\\" + output_filename + "\\" + to_string(i + 1) + ".net";

				boost::filesystem::path dir(output_filename);
				if (boost::filesystem::create_directory(dir) || boost::filesystem::is_directory(dir)) {
					phenotype.saveNeuralNetwork(work_dir + "\\" + output_filename + "\\" + to_string(i + 1) + ".net");

					BOOST_LOG_TRIVIAL(info) << "Reloaded net from file. Retesting:";
					phenotype.loadNeuralNetwork(work_dir + "\\" + output_filename + "\\" + to_string(i + 1) + ".net");
					phenotype.TestNeuralNet();
				} else {
					BOOST_LOG_TRIVIAL(info) << "Error creating folder.";
				}
			}
			BOOST_LOG_TRIVIAL(info) << "";
		}
	}

	BOOST_LOG_TRIVIAL(info) << "--Genetic algorithm finished running.";
	cout << "Finished.";
}