// ForexForecasterDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Net.h"

_DLLAPI double __stdcall Test(double one, double two, double three) {
	return one + two + three;
}

_DLLAPI double __stdcall predictNextPriceFibbed(double* inputs, wchar_t *net_path) {
	Net net(net_path);
	
	vector<double> inputs_vector;
	int input_count = net.getTopology()[0];
	for (size_t i = 0; i < input_count; i++) { 
		inputs_vector.push_back(inputs[i]);
	}

	net.feedForward(inputs_vector);
	return net.getResults()[0];
}